var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
const resizeImg = require('resize-img');

var {
    Validator
} = require('node-input-validator');
require('dotenv').config();
// var session = require('express-session');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images')
    },
    filename: function (req, file, cb) {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});




// Login
router.post('/login', function (req, res, next) {
    const v = new Validator(req.body, {
        email: 'required|email',
        password: 'required'
    });

    v.check().then((matched) => {
        if (!matched) {
            res.status(422).send(v.errors);
        }
    });
    var email = req.body.email;
    var password = req.body.password;
    var obj;

    pool.query("select * from users where email= '" + req.body.email + "' && password='" + req.body.password + "'  ")
        .then(async (rows) => {
            if (rows.length == 1) {
                const access_token = jwt.sign({
                    email: email
                }, process.env.ACCESS_TOKEN_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                })


                var profile_status = "online"
                console.log("id during login " + rows[0].id);
                const rows2 = await pool.query("UPDATE users SET status = '" + profile_status + "' WHERE email = '" + email + "' AND password = '" + password + "'  ");
                
                pool.query("select profile_photo_path from users where id= '" + rows[0].id + "' ")
                    .then((rows3) => {


                        if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = 'build/images/' + rows3[0].profile_photo_path;
                            const bitmap = fs.readFileSync(path);
                            var image = bitmap.toString('base64');
                            rows3[0].profile_photo = "data:image/png;base64," + image;
                        }
                        pool.query("SELECT DISTINCT id FROM messages WHERE reciever_id='"+rows[0].id+"' AND status=0").then(total_chats=>{
                        console.log("total conversations ="+total_chats.length);
                        
                        res.contentType('text/plain');

                        res.send({
                            status: "OK",
                            id: rows[0].id,
                            token: access_token,
                            profile_photo: rows3,
                            conversations:total_chats.length
                        });

                        }).catch(err=>{
                            console.log(err);
                            
                        })

                    })
                    .catch(err => {
                        //handle error
                        console.log(err);

                    })



            } else {
                res.sendStatus(404);

            }
        })
        .catch(err => {
            //handle error
            console.log(err);
            // conn.end();
        })

});






//GET PIC for profile
router.get('/user_pic', verifyToken, function (req, res) {

    pool.query("select profile_photo_path,name,country from users where id= '" + req.query.user_id + "' ")
        .then((rows3) => {
            if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                const path = 'build/images/' + rows3[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows3[0].profile_photo = "data:image/png;base64," + image;
            }
            res.contentType('text/plain');

            res.send({
                status: "OK",
                name: rows3[0].name,
                profile_photo: rows3[0].profile_photo,
                state: rows3[0].country
            });
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
})
// upload.single('profile_photo')
// Create profile
router.post('/create_profile', verifyToken, upload.single('profile_photo'), function (req, res, next) {
    var profile_photo_path = req.file;
    console.log(profile_photo_path);
    if (profile_photo_path == undefined) {
        profile_photo_path = "";
        console.log("File Name= " + profile_photo_path);
    } else {
        profile_photo_path = req.file.filename;
        console.log("File Name =" + profile_photo_path);
        
    var dimensions = sizeOf('public/images/'+profile_photo_path);
    console.log(dimensions.width, dimensions.height);
    var actual_width = dimensions.width;
    var actual_height =dimensions.height;
    var reduced_height,reduced_width;

    if(actual_height>actual_width)
    {
      console.log("height greater"); 
      reduced_width=180;
       var aspect_ratio =  actual_height / actual_width ;
       reduced_height = reduced_width * aspect_ratio
    }
    else if(actual_height<actual_width)
    {
      console.log("width greater");

      reduced_height=180;
      var aspect_ratio =  actual_width / actual_height ;
      reduced_height = reduced_height * aspect_ratio
    }
 
    (async () => {
      const image = await resizeImg(fs.readFileSync('public/images/'+profile_photo_path), {
        
          width: reduced_width,
          height: reduced_height
      });
   console.log("photo="+profile_photo_path);
   
      fs.writeFileSync('build/images/'+profile_photo_path, image);
  })();

    }
    var relationship_status = req.body.relationship_status;
    var education = req.body.education;
    // var country = req.body.country;
    var bio = req.body.bio;
    console.log("bio", bio);
    var languages = req.body.languages;
    // var user_email = req.session.email;
    var lang = languages.toString();
    var id = req.query.id;
    let sql = `UPDATE users
           SET profile_photo_path = ?,
           relationship_status = ?,
           education = ?,
           bio=?, 
           languages=?
           WHERE id = ?`;
    let data = [profile_photo_path, relationship_status, education, bio, languages, id];
    pool.query(sql, data)
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })


});
// edit profile
router.post('/edit_profile', verifyToken, upload.single('profile_photo'), function (req, res, next) {
    var name = req.body.name;

    var profile_photo_path = req.file;
    console.log(profile_photo_path);
    if (profile_photo_path == undefined) {
        profile_photo_path = "";
        console.log("File Name= " + profile_photo_path);
    } else {
        profile_photo_path = req.file.filename;
        console.log("File Name =" + profile_photo_path);
        
    var dimensions = sizeOf('public/images/'+profile_photo_path);
    console.log(dimensions.width, dimensions.height);
    var actual_width = dimensions.width;
    var actual_height =dimensions.height;
    var reduced_height,reduced_width;

    if(actual_height>actual_width)
    {
      console.log("height greater"); 
      reduced_width=180;
       var aspect_ratio =  actual_height / actual_width ;
       reduced_height = reduced_width * aspect_ratio
    }
    else if(actual_height<actual_width)
    {
      console.log("width greater");

      reduced_height=180;
      var aspect_ratio =  actual_width / actual_height ;
      reduced_height = reduced_height * aspect_ratio
    }
 
    (async () => {
      const image = await resizeImg(fs.readFileSync('public/images/'+profile_photo_path), {
        
          width: reduced_width,
          height: reduced_height
      });
   console.log("photo="+profile_photo_path);
   
      fs.writeFileSync('build/images/'+profile_photo_path, image);
  })();
    }
    var password = req.body.password;
    // var gender = req.bodsy.gender;
    var location = req.body.location;
    var day = req.body.day;
    var month = req.body.month;
    var year = req.body.year;
    var bio = req.body.bio;
    var languages = req.body.languages;
    // var profile_photo_path = req.body.profile_photo;
    var relationship_status = req.body.relationship_status;
    var education = req.body.education;
    var country = req.body.country;
    // var user_email = req.session.email;
    var id = req.query.id;

    pool.query("UPDATE users SET year = '" + year + "',month = '" + month + "',day = '" + day + "',location = '" + location + "',name = '" + name + "',profile_photo_path = '" + profile_photo_path + "',relationship_status = '" + relationship_status + "',education = '" + education + "',country = '" + country + "',bio = '" + bio + "',languages = '" + languages + "' WHERE id = '" + id + "' ")
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })
});
// Upload a photo
router.post('/upload_photo', verifyToken, upload.single('image'), function (req, res, next) {

    // var image_name = req.body.image;
    var image_name = req.file.filename;
    console.log(image_name);
    console.log("image name in upload photo " + image_name);
    var id = 0;

    var dimensions = sizeOf('public/images/'+image_name);
    console.log(dimensions.width, dimensions.height);
    var actual_width = dimensions.width;
    var actual_height =dimensions.height;
    var reduced_height,reduced_width;

    if(actual_height>actual_width)
    {
      console.log("height greater"); 
      reduced_width=180;
       var aspect_ratio =  actual_height / actual_width ;
       reduced_height = reduced_width * aspect_ratio
    }
    else if(actual_height<actual_width)
    {
      console.log("width greater");

      reduced_height=180;
      var aspect_ratio =  actual_width / actual_height ;
      reduced_height = reduced_height * aspect_ratio
    }
 
    (async () => {
      const image = await resizeImg(fs.readFileSync('public/images/'+image_name), {
        
          width: reduced_width,
          height: reduced_height
      });
   console.log("photo="+image_name);
   
      fs.writeFileSync('build/images/'+image_name, image);
  })();

    var user_id = req.query.id;
    console.log("User_id in upload photo " + user_id);

    pool.query("INSERT INTO photos value (?,?,?)", [image_name, id, user_id])
        .then((rows) => {
            res.json({
                status: "OK"
            });

        })
        .catch(err => {
            //handle error
            console.log(err);
            res.json({
                status: "NOT Inserted"
            });
            // conn.end();
        })

});
// Get Personal Profile
var data = [];
router.get('/profile', verifyToken, function (req, res, next) {

    var user_id = req.query.id;


    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages from users where id= '" + user_id + "' ")
        .then((rows) => {
            const dt = new Date();
            var current_year = dt.getFullYear();

            rows[0].age = current_year - rows[0].year;

            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                console.log("image: " + rows[0].profile_photo_path);

                const path = 'build/images/' + rows[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows[0].profile_photo = "data:image/png;base64," + image;
            }
            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;
            data = rows[0];
            pool.query("select image_name,id from photos where user_id= '" + user_id + "' ")
                .then((rows) => {

                    var len = rows.length;
                    var i;
                    console.log("Total Media= " + len);
                    for (i = 0; i < len; i++) {
                        if (rows[i].image_name == null || rows[i].image_name == "") {
                            console.log("image not exist");

                        } else {
                            const path = 'build/images/' + rows[i].image_name;
                            const bitmap = fs.readFileSync(path);
                            var image = bitmap.toString('base64');
                            rows[i].media = "data:image/png;base64," + image;
                        }
                    }
                    res.contentType('text/plain');
                    res.send({
                        status: "OK",
                        data: data,
                        photos: rows
                    });

                })
                .catch(err => {
                    //handle error
                    console.log(err);

                })
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});

// Edit profile request

var data = [];
router.get('/edit_personal_profile', verifyToken, function (req, res, next) {

    var user_id = req.query.id;
    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages,status from users where id= '" + user_id + "' ")
        .then((rows) => {
            const dt = new Date();
            var current_year = dt.getFullYear();

            rows[0].age = current_year - rows[0].year;

            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");

            } else {
                const path = 'build/images/' + rows[0].profile_photo_path;
                const bitmap = fs.readFileSync(path);
                var image = bitmap.toString('base64');
                rows[0].profile_photo = "data:image/png;base64," + image;
            }


            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;
            res.contentType('text/plain');
            res.send(rows);
        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});


function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

router.get('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


router.post('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


module.exports = router;
