var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

// Get Specific Profile
router.get('/',verifyToken, function (req, res, next) {

    var user_id = req.query.user_id;
    pool.query("select name,id,location,day,month,year,profile_photo_path,relationship_status,education,country,bio,languages,status from users where id= '" + user_id + "' ")
        .then((rows) => {
            console.log(rows[0]);
            if (rows[0].profile_photo_path == null || rows[0].profile_photo_path == "") {
                console.log("image not exist");

            } else {
                const path = '/compressed/' + rows[0].profile_photo_path;
                rows[0].profile_photo = path;
            }


            var str = rows[0].languages;
            var arr = str.split(",")
            rows[0].languages = arr;
            const dt = new Date();
            var time = dt.getTime();
            var current_year = dt.getFullYear();
            rows[0].age = current_year - rows[0].year;
            pool.query("select image_name,id from photos where user_id= '" + req.query.user_id + "' ")
                .then((media) => {

                    var len = media.length;
                    var i;
                    console.log("Total Media= " + len);
                    for (i = 0; i < len; i++) {
                        if (media[i].image_name == null || media[i].image_name == "") {
                            console.log("image not exist");

                        } else {
                            const path = '/compressed/' + media[i].image_name;
                            media[i].media =path;
                        }
                    }
                    res.contentType('text/plain');
                    res.send({
                        status: "OK",
                        data: rows,
                        photos: media
                    });
                })
                .catch(err => {
                    //handle error
                    console.log(err);

                })

        })
        .catch(err => {
            //handle error
            console.log(err);
        })
});


function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
