var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

// Insert a block useer
router.post('/',verifyToken, function (req, res) {
    var user_1 = req.query.user_1; // user id that blocks the other
    var user_2 = req.query.user_2; // id or user that blocked by user_1
    var id = 0;
    pool.query("INSERT INTO blocks value(?,?,?)", [id, user_1, user_2]).then(rows => {
        res.sendStatus(200);
    }).catch(err => {})

});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
