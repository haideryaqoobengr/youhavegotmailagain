var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

// Filter(search) with specific credentials
router.post('/', verifyToken,function (req, res, next) {
    var user_id = req.query.id;
    var upper_age = req.body.upper_age;
    var lower_age = req.body.lower_age;
    var state = req.body.state;
    var gender = req.body.gender;
    var id = 0;
    console.log("Upper_age: " + upper_age);
    console.log("lower_age: " + lower_age);
    console.log("Gender: " + gender);
    console.log("State: " + state);
    console.log("In search");
    pool.query("SELECT * FROM filters WHERE user_id='" + user_id + "'").then(rows => {
        if (rows.length == 1) {
            console.log("In search route to update filter user_id=" + req.query.id);

            pool.query("UPDATE filters SET  upper_age= '" + upper_age + "',lower_age = '" + lower_age + "',gender = '" + gender + "',state = '" + state + "' WHERE user_id = '" + req.query.id + "' ")
                .then((rows) => {
                    res.json({
                        status: "OK"
                    });

                })
                .catch(err => {
                    //handle error
                    console.log("Error =" + err);
                    // conn.end();
                })
        } else {
            console.log("In search route to insert filter user_id=" + req.query.id);

            pool.query("INSERT INTO filters value (?,?,?,?,?,?)", [id, user_id, upper_age, lower_age, gender, state])
                .then((rows) => {
                    res.json({
                        status: "OK"
                    });
                })
                .catch(err => {
                    //handle error
                    console.log(err);
                    res.json({
                        status: "NOT Inserted"
                    });
                    // conn.end();
                })

        }
    }).catch(err => {
        console.log("Error =" + err);

    })
});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
