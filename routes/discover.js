var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');
var fs = require('fs');


// Get All Profiles Related to age
router.get('/', verifyToken, function (req, res) {
    var query_id = req.query.id;
    console.log("In discover Query id= " + query_id);

    pool.query("SELECT * FROM filters WHERE user_id='" + query_id + "'").then(async rows => {
        console.log("No of Rows of user in filters = " + rows.length);
        if (rows.length == 1) {
            var query_id = req.query.id;
            var upper_age = rows[0].upper_age;
            var lower_age = rows[0].lower_age;
            var state = rows[0].state;
            var gender = rows[0].gender;
            console.log("Upper_age: " + upper_age);
            console.log("lower_age: " + lower_age);
            console.log("Gender: " + gender);
            console.log("State: " + state);
            console.log("In Discover for search");

            if ((upper_age != "") && (lower_age != "") && (gender != "") && (state != "")) {
                console.log("1");

                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "' AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                            if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                                console.log("image not exist");
                            } else {
                                const path = '/compressed/' + rows[i].profile_photo_path;
                                rows[i].profile_photo = path;
                            }
                            arr.push(rows[i])
                        }
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if ((upper_age != "") && (lower_age != "") && (gender != "")) {
                console.log("2");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                            if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                                console.log("image not exist");

                            } else {
                                const path = '/compressed/' + rows[i].profile_photo_path;
                                rows[i].profile_photo = path;
                            }
                            arr.push(rows[i])
                        }
                    }
                }
                res.contentType('text/plain');
                res.send(arr);

            } else if ((upper_age != "") && (lower_age != "") && (state != "")) {
                console.log("3");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "')  AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                            if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                                console.log("image not exist");

                            } else {
                                const path = '/compressed/' + rows[i].profile_photo_path;
                                rows[i].profile_photo = path;
                            }
                            arr.push(rows[i])
                        }
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if ((gender != "") && (state != "")) {
                console.log("4");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where   gender = '" + gender + "' AND country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);

                        if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = '/compressed/' + rows[i].profile_photo_path;
                            rows[i].profile_photo = path;
                        }
                        arr.push(rows[i])
                    }

                }
                res.contentType('text/plain');
                res.send(arr);
            } else if ((upper_age != "") && (lower_age != "")) {
                console.log("5");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "')   ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].age >= lower_age && rows[i].age <= upper_age) {
                            if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                                console.log("image not exist");

                            } else {
                                const path = '/compressed/' + rows[i].profile_photo_path;
                                rows[i].profile_photo = path;
                            }

                            arr.push(rows[i])
                        }


                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else if (gender != "") {
                console.log("6");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND gender = '" + gender + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        // if(rows[i].age >= lower_age && rows[i].age <=upper_age){
                        //     arr.push(rows[i])
                        // }

                        if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = '/compressed/' + rows[i].profile_photo_path;
                            rows[i].profile_photo = path;
                        }
                        arr.push(rows[i])
                    }

                }
                // res.contentType('text/plain');
                res.send(arr);
            } else if (state != "") {
                console.log("7");
                var arr = [];
                var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where  id NOT IN ('" + query_id + "') AND  country = '" + state + "'  ")
                var i;
                const dt = new Date();
                var current_year = dt.getFullYear();
                var len = rows.length;
                var age;
                console.log(len);
                for (i = 0; i < len; i++) {
                    var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                    console.log("Blocked Rows Length= " + block_rows.length);
                    if (block_rows.length == 1) {
                        console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                    } else {
                        rows[i].age = current_year - rows[i].year;
                        console.log("i=" + i);
                        console.log("age=" + rows[i].age);
                        if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                            console.log("image not exist");

                        } else {
                            const path = '/compressed/' + rows[i].profile_photo_path;
                            rows[i].profile_photo = path;
                        }
                        arr.push(rows[i])
                    }
                }
                res.contentType('text/plain');
                res.send(arr);
            } else {
                res.json("Please Fill at least one the Fields")
            }
        } else if (rows.length == 0) {
            var arr = [];
            console.log("in else if statement Query id =" + req.query.id);

            pool.query("select * from users where id= '" + req.query.id + "' ")
                .then(async rows => {
                    console.log("year " + rows[0].year);
                    var year = rows[0].year;
                    var x = parseInt(year) + 10;
                    var y = -10;
                    var rows = await pool.query("select name,id,year,location,email,profile_photo_path,status from users where id NOT IN ('" + req.query.id + "') ")
                    var i;
                    const dt = new Date();
                    var current_year = dt.getFullYear();
                    var len = rows.length;
                    console.log(len);
                    for (i = 0; i < len; i++) {
                        var block_rows = await pool.query("select * from blocks where (blocker='" + req.query.id + "' AND  blocked='" + rows[i].id + "') OR (blocker='" + rows[i].id + "' AND  blocked='" + req.query.id + "')")
                        console.log("Blocked Rows Length= " + block_rows.length);
                        if (block_rows.length == 1) {
                            console.log("Blocked  id= " + rows[i].id + " and user id =" + req.query.id);
                        } else {
                            rows[i].age = current_year - rows[i].year;
                            console.log("i=" + i);
                            console.log("age=" + rows[i].age);
                            if (rows[i].profile_photo_path == null || rows[i].profile_photo_path == "") {
                                console.log("image not exist");

                            } else {
                                const path = '/compressed/' + rows[i].profile_photo_path;
                                rows[i].profile_photo = path;
                            }
                            arr.push(rows[i])
                        }
                    }
                    res.contentType('text/plain');
                    res.send(arr);
                })
                .catch(err => {
                    //handle error
                    console.log(err);
                    // conn.end();
                })

        }


    }).catch(err => {
    })
});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

router.get('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


router.post('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


module.exports = router;
